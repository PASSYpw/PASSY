PASSY
---

[Official hosted version](https://app.passy.pw)

A password manager written in PHP to serve you over the internet.

# Table of Contents
 - [Requirements](#requirements)
 - [Installation](#installation)
   - [Latest](#latest)
   - [Below 2.0.3](#below-203)
 - [Contributing](#contributing)
 - [License](#license)

## Requirements
 - MySQL Server (mysqld, mariadb-server)
 - Web Server (nginx / Apache 2 / lighttpd / ...)
 - PHP 7.0
 - PHP 7.0-openssl module (often included with PHP 7.0)
 - PHP 7.0-mysql module
 - PHP 7.0-json module
 - PHP 7.0-curl module (only needed for ReCaptcha support)
 - [Composer](https://getcomposer.org/download/) (For Ubuntu 16.04 and newer: `apt install composer`)
 - [npm](https://docs.npmjs.com/getting-started/installing-node) (For Ubuntu 14.04 and newer: `apt install npm`)
 - [Yarn](https://yarnpkg.com) (`npm install -g yarn`)
 
## Installation
### Latest
 - [Download](https://gitlab.com/PASSYpw/PASSY/tags) a version of PASSY in your preferred format (zip / tar.gz).
 - Unzip it in your web root.
 - Run the following command: `yarn`
 - Edit the `config.inc.php`

Walkthrough (Ubuntu 16.04.3):
[![asciicast](https://asciinema.org/a/153044.png)](https://asciinema.org/a/153044)

### Below 2.0.3
 - [Download](https://gitlab.com/PASSYpw/PASSY/tags/2.0.3) PASSY in your preferred format (zip / tar.gz).
 - Unzip it in your web root.
 - Run the following command: `composer install`
 - Edit the `config.inc.php`

## Contributing
More information on how to contribute to PASSY can be found under [CONTRIBUTING](CONTRIBUTING.md). Please also refer to the [CODE OF CONDUCT](CODE_OF_CONDUCT.md) file.

## License
This project is licensed under the GNU General Public License v3.
You can find more information about it in the [LICENSE](LICENSE) file.
